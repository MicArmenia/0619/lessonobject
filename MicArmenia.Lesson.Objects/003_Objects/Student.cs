﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _003_Objects
{
    class Student
    {
        public string surname;
        public string email;
        public string name;
        public byte age;

        public string Fullname
        {
            get { return $"{surname} {name}"; }
        }

        //public string GetFullname()
        //{
        //    return $"{surname} {name}";
        //}

        public int Test { get; private set; }
    }
}
