﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _002_Objects
{
    class Program
    {
        static void Main(string[] args)
        {
            Student st = new Student();
            //st.age = 250;
            //st.SetAge(25);
            //int age = st.GetAge();
            st.Age = 25;
            st.Name = "Barev";
            st.Surname = "yan";

            Console.WriteLine($"Age: {st.Age}");
            Console.WriteLine($"Surname: {st.Surname}");
            Console.ReadLine();
        }
    }
}
