﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _002_Objects
{
    class Student
    {
        public string Surname { get; set; }
        public string Email { get; set; }

        //private string _email;
        //public string Email
        //{
        //    get { return _email; }
        //    set { _email = value; }
        //}

        private string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                if (!string.IsNullOrEmpty(value))
                    if (char.IsLetter(value[0]))
                        _name = value;
            }
        }

        private byte _age;
        public byte Age
        {
            get { return _age; }
            set
            {
                if (value < 15 || value > 70)
                    _age = 0;
                else
                    _age = value;
            }
        }

        //public void set_Age(byte value)
        //{
        //    if (value < 15 || value > 70)
        //        _age = 0;
        //    else
        //        _age = value;
        //}

        //public byte get_Age()
        //{
        //    return _age;
        //}
    }
}
