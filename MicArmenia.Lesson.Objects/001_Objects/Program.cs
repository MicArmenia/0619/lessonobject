﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _001_Objects
{
    class Program
    {
        static void Main(string[] args)
        {
            //Student st = new Student(
            //    name: "A1", 
            //    surname: "A1yan", 
            //    email: "a1.a1yan@gmail.com", 
            //    age: 20);
            
            //Student st = new Student();
            //st.name = "A1";
            //st.surname = "A1yan";
            //st.email = "a1.a1yan@gmail.com";
            //st.age = 20;

            Student st = new Student()
            {
                //zname = "00A1",
                surname = "A1yan",
                age = 20,
                email = "a1.a1yan@gmail.com"
            };

            //st.name = "001";
            
            Console.ReadLine();
        }
    }
}
