﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _001_Objects
{
    class Student
    {
        #region Constructors

        public Student()
        { }

        public Student(string name)
        {
            //if (name != null && name.Length > 0)
            if (!string.IsNullOrEmpty(name))
                if (char.IsLetter(name[0]))
                    this.name = name;
        }

        //public Student(string name, string surname)
        //    : this(name)
        //{
        //    this.surname = surname;
        //}

        //public Student(string name, string surname, string email)
        //    : this(name, surname)
        //{
        //    this.email = email;
        //}

        //public Student(string name, string surname, string email, int age)
        //    : this(name, surname, email)
        //{
        //    this.age = age;
        //}

        #endregion

        public readonly string name;
        public string surname;
        public string email;
        public int age;
    }
}